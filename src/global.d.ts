import XtxSkeleton from '@/components/skeleton/index.vue'
import XtxCarousel from '@/components/carousel/index.vue'
import XtxBread from '@/components/bread/index.vue'
import XtxBreadItem from '@/components/breadItem/index.vue'
import XtxCity from '@/components/city/index.vue'
import XtxNumbox from '@/components/number/index.vue'
import XtxButton from '@/components/button/index.vue'
import XtxCheckbox from '@/components/checkbox/index.vue'
import XtxMessage from '@/components/message/index.vue'
declare module 'vue' {
  export interface GlobalComponents {
    // key 是 全局组件名
    // value 是 对应的组件类型
    XtxSkeleton: typeof XtxSkeleton
    XtxCarousel: typeof XtxCarousel
    XtxBreadItem: typeof XtxBreadItem
    XtxCity: typeof XtxCity
    XtxNumbox: typeof XtxNumbox
    XtxButton: typeof XtxButton
    XtxCheckbox: typeof XtxCheckbox
    XtxMessage: typeof XtxMessage
  }
}
