type Atype = {
  name: string
  age: number
}

type readonlyA = Readonly<Atype>
const a: Atype = {
  name: '张三',
  age: 19
}

// a.name = '李四'
