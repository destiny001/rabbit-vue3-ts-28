/// <reference types="vite/client" />

declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>
  export default component
}
// 亮点+难点
type GetMeCb = (openId: string) => void

namespace QC {
  // 类型声明
  declare const Login: {
    check: () => boolean
    getMe: (GetMeCb) => void
  }
  // 类型声明
  declare const api: (apiStr: string) => any
}
