export type UserType = {
  account: string
  avatar: string
  birthday: string
  cityCode: string
  gender: string
  id: string
  mobile: string
  nickname: string
  profession: string
  provinceCode: string
  token: string
}

export type QqUserInfo = {
  figureurl_qq_2: string
  nickname: string
}
