// hooks函数一般都以use开头
// 用来进行复用状态和逻辑
import { useIntervalFn } from '@vueuse/core'
import { useIntersectionObserver } from '@vueuse/core'
import { ref } from 'vue'
// 封装懒加载数据
export function useLazyData(callback: () => void) {
  const target = ref(null)

  const { stop } = useIntersectionObserver(target, ([{ isIntersecting }]) => {
    if (isIntersecting) {
      // 做你想做的事情
      callback()
      stop()
    }
  })
  return target
}

// 倒计时的hooks
export function useCountDown(count: number) {
  const time = ref(0)
  const { pause, resume } = useIntervalFn(
    () => {
      time.value--
      if (time.value === 0) {
        pause()
      }
    },
    1000,
    {
      immediate: false
    }
  )
  const start = () => {
    time.value = count
    resume()
  }
  return {
    time,
    start
  }
}
