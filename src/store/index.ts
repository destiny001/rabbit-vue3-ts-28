import categroy from './modules/categroy'
import home from './modules/home'
import goods from './modules/goods'
import user from './modules/user'

export default function useStore() {
  return {
    categroyStore: categroy(),
    homeStore: home(),
    goodsStore: goods(),
    userStore: user()
  }
}
